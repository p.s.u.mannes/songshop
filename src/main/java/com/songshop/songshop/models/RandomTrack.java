package com.songshop.songshop.models;

// Class to store random track information for the homepage
public class RandomTrack {
    private String artist;
    private String track;
    private String genre;

    public RandomTrack(String artist, String track, String genre) {
        this.artist = artist;
        this.track = track;
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
