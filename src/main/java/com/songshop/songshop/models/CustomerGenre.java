package com.songshop.songshop.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Class to store genres for display of most popular genres method
public class CustomerGenre {

    private ArrayList<String> genres;

    public CustomerGenre(ArrayList<String> genres) {
        this.genres = genres;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return "CustomerGenre{" +
                "genres=" + genres +
                '}';
    }
}
