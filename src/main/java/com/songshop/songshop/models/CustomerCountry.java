package com.songshop.songshop.models;

import java.util.HashMap;

// Class to store customer amounts by country
public class CustomerCountry {

    private String countryName;
    private int customerAmount;

    public CustomerCountry(String countryName, int customerAmount) {
        this.countryName = countryName;
        this.customerAmount = customerAmount;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCustomerAmount() {
        return customerAmount;
    }

    public void setCustomerAmount(int customerAmount) {
        this.customerAmount = customerAmount;
    }
}
