package com.songshop.songshop.dbhelper;

import com.songshop.songshop.models.*;

import java.util.ArrayList;

public interface CustomerRepository {
     // List of all methods in CustomerRepositoryImpl
     ArrayList<Customer> selectAllCustomers();
     Customer selectSpecificCustomerById(int customerId);
     Customer selectSpecificCustomerByName(String firstName, String lastName);
     ArrayList<Customer> selectCustomersPage(int page);
     boolean addCustomer(Customer customer);
     boolean updateCustomer(Customer customer);
     ArrayList<CustomerCountry> getCustomerAmountByCountry();
     ArrayList<CustomerSpender> getSpenderByTotalInvoice();
     CustomerGenre getFavouriteGenre(int customerId);
     ArrayList<RandomTrack> getRandomTracks();
     ArrayList<SearchResult> getSearchResult(String query);
}
