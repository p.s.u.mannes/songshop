package com.songshop.songshop.logging;

import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

// Class responsible for logging exceptions in
// CustomerRepositoryImpl
@Service
public class LogToConsole {
    public void log(String message){
        // Display date
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        // Print the error
        System.out.println(dateFormat.format(date) + " : " + message); //2016/11/16 12:08:43
    }
}
