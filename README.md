![](songshop.png)

<h1 align="center">Welcome to SongShop 👋</h1>

> An app to display random songs and search for songs
> in a database

### 🏠 [https://songshop-500.herokuapp.com/](https://songshop-500.herokuapp.com/)

## Usage

```sh
    Accesses Chinook database using jdbc
    Download dependencies if necessary
    
    Compile using Maven to .jar
    Run the .jar
```

## Run tests

```sh
    Run test folder in Intellij
```

Usage:
Type in a song in the search bar and press search.
If it is found in the database, the user is taken to the
results page.


Limitations:
-> Uses SQLite in Heroku deployment, changes are not
persistent
-> Limited functionality


***
_END OF README FILE_
